import 'package:flutter/material.dart';
import 'package:my_trello/screens/board_detail_screen.dart';
import 'package:my_trello/screens/dashboard_screen.dart';
import 'package:my_trello/screens/greeting_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Trello',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: const Color(0xff0079BF),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: Color(0xff61BD4F),
        ),
        iconTheme: IconThemeData(
          color: Color(0xff182B4E),
        ),
        textTheme: TextTheme(
          button: TextStyle(
            color: Color(0xff519839),
          ),
        ),
        primarySwatch: Colors.blue,
        backgroundColor: Colors.grey[100],
      ),
      initialRoute: GreetingScreen.routeName,
      routes: {
        BoardDetailScreen.routeName: (context) => const BoardDetailScreen(),
        GreetingScreen.routeName: (context) => const GreetingScreen(),
        DashboardScreen.routeName: (context) => const DashboardScreen()
      },
    );
  }
}
