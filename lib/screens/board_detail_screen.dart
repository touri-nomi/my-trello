import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_trello/models/card.dart' as CardModel;

class BoardDetailScreen extends StatefulWidget {
  static const routeName = "/board";
  const BoardDetailScreen({Key? key}) : super(key: key);

  @override
  State<BoardDetailScreen> createState() => _BoardDetailScreenState();
}

class _BoardDetailScreenState extends State<BoardDetailScreen> {
  final List _lists = [
    {"title": "List1", "cards": []},
    {
      "title": "List2",
      "cards": <CardModel.Card>[
        CardModel.Card(title: "Card1"),
        CardModel.Card(title: "Card2"),
        CardModel.Card(title: "Card3"),
        CardModel.Card(title: "Card4"),
      ],
    },
  ];
  static const double LIST_WIDTH = 300;

  DragAndDropItem _renderCard(CardModel.Card card) {
    return DragAndDropItem(
      child: Container(
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.symmetric(horizontal: 8),
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 0,
                blurRadius: 2,
                offset: Offset(0, 1), // changes position of shadow
              ),
            ],
            color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              card.title ?? "",
              style: TextStyle(fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }

  DragAndDropList _renderList(list) {
    // return Container(
    //   margin: EdgeInsets.all(16),
    //   width: LIST_WIDTH,
    //   padding: EdgeInsets.all(8),
    //   decoration: BoxDecoration(
    //       color: Theme.of(context).backgroundColor,
    //       borderRadius: BorderRadius.all(
    //         Radius.circular(8),
    //       ),
    //       boxShadow: [
    //         BoxShadow(
    //           color: Colors.grey.withOpacity(0.5),
    //           spreadRadius: 0,
    //           blurRadius: 2,
    //           offset: Offset(0, 1), // changes position of shadow
    //         ),
    //       ]),
    //   child: Column(
    //     children: [
    //       Padding(
    //         padding: const EdgeInsets.all(8.0),
    //         child: Row(
    //           children: [
    //             Text(
    //               list["title"],
    //               style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
    //             ),
    //           ],
    //         ),
    //       ),
    //       SizedBox(
    //         height: 8,
    //       ),
    //       Expanded(
    //         child: () {
    //           if (list!.cards != null) {
    //             return ListView.separated(
    //               itemCount: (list.cards as List).length,
    //               itemBuilder: (context, index) =>
    //                   _renderCard(list['cards'][index]),
    //               separatorBuilder: (context, index) => SizedBox(height: 10),
    //             );
    //           }
    //           return Container();
    //         }(),
    //       ),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.start,
    //         children: [
    //           TextButton(
    //             onPressed: () {},
    //             child: Row(children: [
    //               Icon(
    //                 Icons.add,
    //                 color: Theme.of(context)
    //                     .floatingActionButtonTheme
    //                     .backgroundColor,
    //               ),
    //               Text(
    //                 "Add card",
    //                 style: TextStyle(
    //                   color: Theme.of(context)
    //                       .floatingActionButtonTheme
    //                       .backgroundColor,
    //                 ),
    //               ),
    //             ]),
    //           ),
    //         ],
    //       )
    //     ],
    //   ),
    // );

    return DragAndDropList(
      contentsWhenEmpty: null,
      footer: TextButton(
        onPressed: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextButton(
              onPressed: () {},
              child: Row(children: [
                Icon(
                  Icons.add,
                  color: Theme.of(context)
                      .floatingActionButtonTheme
                      .backgroundColor,
                ),
                Text(
                  "Add card",
                  style: TextStyle(
                    color: Theme.of(context)
                        .floatingActionButtonTheme
                        .backgroundColor,
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
      header: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          list["title"],
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      children: [
        ...(list["cards"] as List).map((card) => _renderCard(card)).toList(),
      ],
    );
  }

  _onItemReorder(
      int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    setState(() {
      var movedItem =
          (_lists[oldListIndex]?["cards"] as List).removeAt(oldItemIndex);
      (_lists[newItemIndex]?["cards"] as List).insert(newItemIndex, movedItem);
    });
  }

  _onListReorder(int oldListIndex, int newListIndex) {
    setState(() {
      var movedList = _lists.removeAt(oldListIndex);
      _lists.insert(newListIndex, movedList);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text("Board1"),
        ),
        // body: PageView(
        //   padEnds: false,
        //   controller: PageController(viewportFraction: 0.75),
        //   children: [
        //     ..._lists.map((list) => _renderList(list)),
        //     _renderListCreator(),
        //   ],
        // ),
        body: Container(
          child: DragAndDropLists(
            itemDivider: SizedBox(
              height: 8,
            ),
            listDividerOnLastChild: false,
            listWidth: LIST_WIDTH,
            listDecoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.all(Radius.circular(7.0)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black45,
                  spreadRadius: 3.0,
                  blurRadius: 6.0,
                  offset: Offset(2, 3),
                ),
              ],
            ),
            listPadding: EdgeInsets.all(8),
            axis: Axis.horizontal,
            children: _lists.map((list) => _renderList(list)).toList(),
            onItemReorder: _onItemReorder,
            onListReorder: _onListReorder,
          ),
        ));
  }
}
