import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:my_trello/screens/board_detail_screen.dart';

class DashboardScreen extends StatefulWidget {
  static const routeName = "/dashboard";
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final List _starredBoards = [
    {"name": "Board1", "imageUrl": "http://via.placeholder.com/200"},
    {"name": "Board2", "imageUrl": "http://via.placeholder.com/200"},
    {"name": "Board3", "imageUrl": "http://via.placeholder.com/200"},
  ];

  final List _workspaces = [
    {
      "name": "Workspace1",
      "boards": [
        {"name": "Board1", "imageUrl": "http://via.placeholder.com/200"},
        {"name": "Board2", "imageUrl": "http://via.placeholder.com/200"},
        {"name": "Board3", "imageUrl": "http://via.placeholder.com/200"},
      ]
    },
    {
      "name": "Workspace2",
      "boards": [
        {"name": "Board1", "imageUrl": "http://via.placeholder.com/200"},
        {"name": "Board2", "imageUrl": "http://via.placeholder.com/200"},
        {"name": "Board3", "imageUrl": "http://via.placeholder.com/200"},
      ]
    },
  ];

  void goToBoard() {
    Navigator.pushNamed(context, BoardDetailScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    IconButton _renderActionBtn({required icon, onPressed}) {
      return IconButton(
        onPressed: onPressed,
        icon: icon,
        splashRadius: 24,
      );
    }

    Widget _renderCardBoard(board) {
      return ClipRRect(
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        child: Stack(clipBehavior: Clip.hardEdge, children: [
          Container(
            height: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(board["imageUrl"]),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.black.withOpacity(0.4),
              padding: const EdgeInsets.all(12),
              child: Text(
                board["name"],
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ]),
      );
    }

    Widget _renderListItemBoard(board) {
      return ListTile(
        onTap: () => goToBoard(),
        leading: Container(
          width: 48,
          height: 48,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            image: DecorationImage(
              image: NetworkImage(
                board["imageUrl"],
              ),
            ),
          ),
        ),
        title: Text(board["name"]),
      );
    }

    Widget _renderWorkspaceHeader(title) {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.symmetric(
            horizontal: BorderSide(width: 1, color: Colors.black12),
          ),
        ),
        child: Text(
          title,
          style: TextStyle(color: Colors.black87),
        ),
      );
    }

    Widget _renderWorkspace(workspace) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _renderWorkspaceHeader(workspace["name"]),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: (workspace['boards'] as List)
                  .map((board) => _renderListItemBoard(board))
                  .toList(),
            ),
          )
        ],
      );
    }

    Drawer _renderDrawer() {
      Widget _getUserProfile() {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage("http://via.placeholder.com/32"),
                radius: 24,
              ),
              SizedBox(
                height: 12,
              ),
              DefaultTextStyle(
                  style: TextStyle(color: Colors.white, height: 1.4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Đinh Nguyễn Nhật Tùng",
                        maxLines: 1,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "@touri_nomi",
                        maxLines: 1,
                      ),
                      Text(
                        "nhattung1709@gmail.com",
                        maxLines: 1,
                      ),
                    ],
                  ))
            ],
          ),
        );
      }

      final List navGroups = [
        {
          'items': [
            {
              'title': 'Boards',
              'icon': Icons.dashboard,
            },
            {'title': "Home", 'icon': Icons.home},
          ]
        },
        {
          'name': 'Workspaces',
          'items': [
            {'title': 'Trello workspace', 'icon': Icons.people}
          ]
        },
        {
          'items': [
            {'title': "My cards", 'icon': Icons.video_label},
            {'title': 'Settings', 'icon': Icons.settings},
            {'title': 'Help', 'icon': Icons.info}
          ]
        }
      ];

      return Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _getUserProfile(),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.expand_more, color: Colors.white),
                    splashRadius: 24,
                  ),
                ],
              ),
            ),
            ...navGroups.map(
              (group) {
                final index = navGroups.indexOf(group);
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (group["name"] != null)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Text(
                              group["name"],
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ...(group["items"] as List)
                        .map((item) => ListTile(
                              leading: Icon(
                                item["icon"],
                                color: Theme.of(context).iconTheme.color,
                              ),
                              title: Text(item["title"]),
                            ))
                        .toList(),
                    if (index != navGroups.length - 1)
                      const Divider(
                        color: Colors.black45,
                      )
                  ],
                );
              },
            ).toList()
          ],
        ),
      );
    }

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        floatingActionButton: SpeedDial(
          closeDialOnPop: true,
          spacing: 8,
          spaceBetweenChildren: 8,
          backgroundColor:
              Theme.of(context).floatingActionButtonTheme.backgroundColor,
          icon: Icons.add,
          children: [
            SpeedDialChild(
              child: const Icon(Icons.dashboard),
              foregroundColor: Colors.white,
              backgroundColor:
                  Theme.of(context).floatingActionButtonTheme.backgroundColor,
              label: 'Board',
              onTap: () {},
            ),
            SpeedDialChild(
              child: const Icon(Icons.video_label),
              foregroundColor: Colors.white,
              backgroundColor:
                  Theme.of(context).floatingActionButtonTheme.backgroundColor,
              label: 'Card',
              onTap: () {},
            ),
          ],
        ),
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: const Text("Boards"),
          actions: [
            _renderActionBtn(onPressed: () {}, icon: const Icon(Icons.search)),
            _renderActionBtn(
                onPressed: () {},
                icon: const Icon(Icons.notifications_outlined))
          ],
        ),
        drawer: _renderDrawer(),
        body: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _renderWorkspaceHeader("Starred boards"),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 16,
                      mainAxisSpacing: 16,
                      mainAxisExtent: 100,
                    ),
                    itemCount: _starredBoards.length,
                    itemBuilder: (context, index) {
                      return _renderCardBoard(_starredBoards[index]);
                    },
                  ),
                )
              ],
            ),
            ..._workspaces
                .map((workspace) => _renderWorkspace(workspace))
                .toList(),
          ],
        ));
  }
}
