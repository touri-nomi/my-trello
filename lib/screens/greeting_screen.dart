import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_trello/components/carousel_with_indicator.dart';
import 'package:my_trello/screens/dashboard_screen.dart';

class GreetingScreen extends StatelessWidget {
  static const routeName = "/greeting";
  const GreetingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List slideContent = [
      {
        "imageUrl": "http://via.placeholder.com/200",
        "title": "Get more done with Trello",
        "subtitle":
            "Plan, track, and organize pretty much anything with anyone."
      },
      {
        "imageUrl": "http://via.placeholder.com/200",
        "title": "Get organized",
        "subtitle": "Make a Trello board to organize anything with anyone."
      },
      {
        "imageUrl": "http://via.placeholder.com/200",
        "title": "Add details",
        "subtitle": "Open cards to add pictures, checklists, and more."
      },
      {
        "imageUrl": "http://via.placeholder.com/200",
        "title": "Team up",
        "subtitle": "Invite people to join your board, all for free."
      }
    ];

    void _handleLogin() {
      Navigator.pushNamed(context, DashboardScreen.routeName);
    }

    ElevatedButton _renderButton({required label, onPressed}) {
      return ElevatedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(
            EdgeInsets.symmetric(horizontal: 20, vertical: 16),
          ),
          backgroundColor: MaterialStateProperty.all(
            Colors.greenAccent[700],
          ),
        ),
        onPressed: onPressed,
        child: Text(
          label,
          style: TextStyle(fontSize: 12, color: Colors.white),
        ),
      );
    }

    final hintTextStyle = TextStyle(color: Colors.grey[400], fontSize: 12);
    final highlightedTextStyle = TextStyle(color: Colors.white, fontSize: 12);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 48),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: CarouselWithIndicator(
                  slides: slideContent.map((data) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Column(
                          children: [
                            Container(
                              height: 200,
                              // width: double.infinity,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(data["imageUrl"]),
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 32,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: Column(
                                children: [
                                  Text(
                                    data["title"],
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(height: 8),
                                  Text(data["subtitle"],
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                      ),
                                      textAlign: TextAlign.center),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                    );
                  }).toList(),
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _renderButton(label: "Sign up"),
                      _renderButton(label: "Log in", onPressed: _handleLogin),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Text.rich(
                    TextSpan(children: [
                      TextSpan(
                          text: "By creating an account, you agree to our ",
                          style: hintTextStyle),
                      TextSpan(
                          text: "Terms of Service",
                          style: highlightedTextStyle),
                      TextSpan(text: " and ", style: hintTextStyle),
                      TextSpan(
                          text: "Privacy Policy", style: highlightedTextStyle)
                    ]),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 24),
                  Text("Contact support", style: highlightedTextStyle),
                  SizedBox(
                    height: 16,
                  )
                ],
              ),
            ]),
      ),
    );
  }
}
